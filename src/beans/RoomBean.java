package beans;

public class RoomBean {

	private int roomId;

	public int getRoomId() {
		return roomId;
	}

	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}

	private String roomType;
	private String roomName;
	private String[] invitation;

	public String getRoomType() {
		return roomType;
	}

	public void setRoomType(String string) {
		if (string.equals("off") || string.equals("PRIVATE")) {
			this.roomType = "PRIVATE";
		} else {
			this.roomType = "PUBLIC";
		}
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String[] getInvitation() {
		return invitation;
	}

	public void setInvitation(String[] strings) {
		this.invitation = strings;
	}

}
