package beans;

public class LoadingMessageBean {
	private String loadingMessage;

	public LoadingMessageBean() {

	}

	public String getLoadingMessage() {
		return loadingMessage;
	}

	public LoadingMessageBean(String loadingMessage) {
		this.loadingMessage = loadingMessage;
	}
}
