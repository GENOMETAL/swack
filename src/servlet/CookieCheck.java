package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserBean;

/**
 * Servlet implementation class LoginCheck
 */
@WebServlet("/CookieCheck")
public class CookieCheck extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			UserBean userBean = new UserBean();
			for (int i = 0; i < cookies.length; i++) {
				if (cookies[i].getName().equals("mail")) {
					System.out.println("ログイン情報を記録しています");
					userBean.setMail(cookies[i].getValue());
				} else if (cookies[i].getName().equals("password")) {
					System.out.println("ログイン情報を記録していますm");
					System.out.println("a");
					userBean.setPassword(cookies[i].getValue());
				}
			}
			request.setAttribute("userBean", userBean);
			getServletContext().getRequestDispatcher("/Login").forward(request, response);
			return;
		}
		System.out.println("ログイン情報を記録していません");
		getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
	}

}
