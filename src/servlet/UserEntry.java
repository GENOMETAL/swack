package servlet;

import static parameter.Messages.*;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserBean;
import dao.UserDAO;
import exception.DatabaseException;

/**
 * Servlet implementation class UserEntry
 */
@WebServlet("/UserEntry")
public class UserEntry extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UserBean userBean = new UserBean();
		userBean.setName(request.getParameter("name"));
		userBean.setMail(request.getParameter("mail"));
		userBean.setPassword(request.getParameter("password"));

		if (!userBean.getErrMsg().equals("")) {
			request.setAttribute("userBean", userBean);
			getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
			return;
		}

		UserDAO userDAO = new UserDAO();
		try {

			if (userDAO.select(userBean)) {
				userBean.setErrMsg(USER_ISENTRYED);
				request.setAttribute("userBean", userBean);
				getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
				return;
			}

			userDAO.register(userBean);
		} catch (DatabaseException e) {
			e.printStackTrace();

			request.setAttribute("exceptionMsg", e.getMessage());
			getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
			return;

		}

		request.setAttribute("infoMsg", USER_ENTRY_SUCCESS);
		getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
	}

}
