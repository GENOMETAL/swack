
package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ChatBean;
import beans.LoginSession;
import dao.ChatDAO;
import exception.DatabaseException;

@WebServlet("/ChatEntry")
public class ChatEntry extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String room = request.getParameter("roomid");
		System.out.println("roomid:" + room);
		LoginSession loginSession = (LoginSession) session.getAttribute("loginSession");
		ChatBean chatBean = new ChatBean();
		chatBean.setRoomId(Integer.parseInt(room));
		System.out.println("chatBean.getRoomID" + chatBean.getRoomId());
		chatBean.setName(loginSession.getName());
		chatBean.setMessage(request.getParameter("message"));
		ChatDAO chatDAO = new ChatDAO();
		try {
			chatDAO.register(chatBean);
		} catch (DatabaseException e) {
			e.printStackTrace();
		}
		System.out.println("ChatEntryのSendRedirect（Index)前");
		response.sendRedirect("Index?room=" + room); //11/05 チャット送信し後にgetメソッドでIndex.Javaに飛ぶため米ンとOUT
		//getServletContext().getRequestDispatcher("/Index").forward(request, response);
	}
}