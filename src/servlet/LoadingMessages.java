package servlet;

import java.io.IOException;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.LoadingMessageBean;

@WebServlet("/LoadingMessages")
public class LoadingMessages extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//		String[] loadingMessageArray = { "お金玉で(^^♪", "あなる大好き", "アナリスト(アナル大好き)" };
		//		String loadingMessage;
		//
		//		Random random = new Random();
		//		int index = random.nextInt(3);
		//		loadingMessage = loadingMessageArray[index];
		//		LoadingMessageBean loadingMessageBean = new LoadingMessageBean(loadingMessage);
		//
		//		request.setAttribute("loadingMessageBean", loadingMessageBean);
		//
		//		getServletContext().getRequestDispatcher("/WEB-INF/index.jsp")
		//				.forward(request, response);
		doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String[] loadingMessageArray = { "あなる大好きなるほど君", "ヤル気！元気！性器！", "ロードしてまちゅよ～♡", "ティッシュを拾い中・・・", "パンツを洗濯中・・・", "いじいじしています。" };
		String loadingMessage;

		Random random = new Random();
		int index = random.nextInt(loadingMessageArray.length);
		loadingMessage = loadingMessageArray[index];
		LoadingMessageBean loadingMessageBean = new LoadingMessageBean(loadingMessage);

		request.setAttribute("loadingMessageBean", loadingMessageBean);

		getServletContext().getRequestDispatcher("/WEB-INF/index.jsp")
				.forward(request, response);
	}
}
//public class LoadingMessages {
//	String[] lodingMessageArray = { "お金玉で(^^♪", "あなる大好き" };
//	String lodingMessage;
//
//	public String randomMessage() {
//		Random random = new Random();
//		int index = random.nextInt(2);
//		lodingMessage = lodingMessageArray[index];
//		return lodingMessage;
//	}
//}
