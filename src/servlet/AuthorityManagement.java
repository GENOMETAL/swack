package servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDAO;
import exception.DatabaseException;

/**
 * Servlet implementation class AuthorityManagement
 */
@WebServlet("/AuthorityManagement")
public class AuthorityManagement extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String[] userBeanList = request.getParameterValues("approval");
		String[] allUserBeanList = request.getParameterValues("ban");
		UserDAO userDAO = new UserDAO();
		if (userBeanList != null) {
			System.out.println("userBeanListとおった");
			for (int i = 0; i < userBeanList.length; i++) {
				try {
					userDAO.delete_Ban(userBeanList[i]);
				} catch (DatabaseException | SQLException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
			}
		}
		if (allUserBeanList != null) {
			for (int i = 0; i < allUserBeanList.length; i++) {
				try {
					userDAO.add_Ban(allUserBeanList[i]);
				} catch (DatabaseException | SQLException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
				System.out.println(allUserBeanList[i]);
			}
		}
		getServletContext().getRequestDispatcher("/Index").forward(request, response);
	}

}
