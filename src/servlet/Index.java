package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ChatBean;
import beans.JoinUserBean;
import beans.LoginSession;
import beans.RoomBean;
import dao.ChatDAO;
import dao.RoomDAO;
import exception.DatabaseException;

@WebServlet("/Index")
public class Index extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//		ChatDAO chatDAO = new ChatDAO();
		//		RoomDAO roomDAO = new RoomDAO();
		//		ArrayList<ChatBean> chatBeanList = null;
		//		ArrayList<RoomBean> roomBeanList = null;
		//		RoomBean roomBean = null;
		//		try {
		//			chatBeanList = chatDAO.selectAll();
		//			//roomBeanList = roomDAO.selectAll();
		//			HttpSession session = request.getSession();
		//			LoginSession loginSession = (LoginSession) session.getAttribute("loginSession");
		//			roomBeanList = roomDAO.select(loginSession);
		//
		//		} catch (DatabaseException e) {
		//			e.printStackTrace();
		//		}
		String room = request.getParameter("room");
		request.setAttribute("room", room);
		//		request.setAttribute("chatBeanList", chatBeanList);
		//		request.setAttribute("roomBeanList", roomBeanList);
		//		getServletContext().getRequestDispatcher("/LoadingMessages").forward(request, response);
		doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String room = "0";
		if (request.getParameter("room") != null) {
			room = request.getParameter("room");
		}
		String roomId = request.getParameter("room");
		System.out.println("ルーム→" + roomId);
		ChatDAO chatDAO = new ChatDAO();
		RoomDAO roomDAO = new RoomDAO();
		ArrayList<ChatBean> chatBeanList = null;
		ArrayList<RoomBean> roomBeanList = null;
		RoomBean roomBean = null;
		JoinUserBean joinUserBean = null;
		try {
			chatBeanList = chatDAO.selectAll(roomId);
			//roomBeanList = roomDAO.selectAll();
			HttpSession session = request.getSession();
			LoginSession loginSession = (LoginSession) session.getAttribute("loginSession");
			roomBeanList = roomDAO.select(loginSession);
			roomBean = roomDAO.roomNameGetting(roomId);
			//chatBeanList = chatDAO.select(roomId);
		} catch (DatabaseException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		request.setAttribute("chatBeanList", chatBeanList);
		request.setAttribute("roomBean", roomBean);
		request.setAttribute("roomBeanList", roomBeanList);
		getServletContext().getRequestDispatcher("/LoadingMessages").forward(request, response);
	}
}