package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.JoinUserBean;
import beans.RoomBean;
import dao.JoinUserDAO;
import dao.RoomDAO;
import exception.DatabaseException;

@WebServlet("/RoomJoin")
public class RoomJoin extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JoinUserBean joinUserBean = new JoinUserBean();
		RoomBean roomBean = new RoomBean();
		RoomDAO roomDAO = new RoomDAO();
		if (request.getParameter("roomType") == null) {
			roomBean.setRoomType("off");
		} else {
			roomBean.setRoomType(request.getParameter("roomType"));
		}
		roomBean.setRoomName(request.getParameter("roomName"));
		roomBean.setInvitation(request.getParameterValues("inviteCnt"));
		System.out.println("人数→" + roomBean.getInvitation());

		System.out.println("roomBean.getRoomType()の中は" + roomBean.getRoomType());
		if (roomBean.getRoomType() == "PRIVATE") {
			System.out.println("プライベートです。");
		} else {
			System.out.println("パブリックです。");
		}
		String[] arrayUser = request.getParameterValues("inviteCnt");
		for (int i = 0; i < roomBean.getInvitation().length; i++) {
			System.out.println(roomBean.getInvitation()[i]);
			try {
				System.out.println("aaaaaaaaaaaaaaa" + request.getParameter("roomId"));
				joinUserBean.setRoomId(Integer.parseInt(request.getParameter("roomId")));
				joinUserBean.setUserId(Integer.parseInt(arrayUser[i]));
				System.out.println(joinUserBean.getRoomId());
				System.out.println(joinUserBean.getUserId());
				JoinUserDAO.register(joinUserBean);
			} catch (DatabaseException e) {
				e.printStackTrace();
			}
		}
		System.out.println("rooooooooooooooooooooom" + request.getParameter("roomId"));
		request.setAttribute("room", request.getAttribute("roomId"));
		getServletContext().getRequestDispatcher("/Index").forward(request, response);
	}
}
