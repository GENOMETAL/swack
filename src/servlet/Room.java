
package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.RoomBean;
import beans.UserBean;
import dao.RoomDAO;
import dao.UserDAO;
import exception.DatabaseException;

@WebServlet("/Room")
public class Room extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UserDAO userDAO = new UserDAO();
		RoomDAO roomDAO = new RoomDAO();
		ArrayList<UserBean> userBeanList = null;
		ArrayList<RoomBean> roomBeanList = null;
		try {
			userBeanList = userDAO.selectAll();
			System.out.println(userBeanList);
			roomBeanList = roomDAO.selectAll();
		} catch (DatabaseException e) {
			e.printStackTrace();
		}
		request.setAttribute("userBeanList", userBeanList);
		request.setAttribute("roomBeanList", roomBeanList);
		getServletContext().getRequestDispatcher("/WEB-INF/room.jsp").forward(request, response);
	}
}