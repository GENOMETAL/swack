
package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.RoomBean;
import beans.UserBean;
import dao.RoomDAO;
import dao.UserDAO;
import exception.DatabaseException;

@WebServlet("/Member")
public class Member extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("メンバーサーブレット" + request.getParameter("roomId"));
		UserDAO userDAO = new UserDAO();
		RoomDAO roomDAO = new RoomDAO();
		ArrayList<UserBean> userBeanList = null;
		ArrayList<RoomBean> roomBeanList = null;
		RoomBean roomBean = new RoomBean();
		try {
			userBeanList = userDAO.selectAll();
			roomBeanList = roomDAO.selectAlldayo();
		} catch (DatabaseException e) {
			e.printStackTrace();
		}
		roomBean.setRoomId(Integer.parseInt(request.getParameter("roomId")));
		request.setAttribute("userBeanList", userBeanList);
		request.setAttribute("roomBean", roomBean);
		request.setAttribute("roomBeanList", roomBeanList);
		getServletContext().getRequestDispatcher("/WEB-INF/member.jsp").forward(request, response);
	}
}