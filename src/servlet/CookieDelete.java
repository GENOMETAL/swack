package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class CookieDelete
 */
@WebServlet("/CookieDelete")
public class CookieDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Cookie cookies[] = request.getCookies();

		for (int i = 0; i < cookies.length; i++) {
			if (cookies[i].getName().equals("JSESSIONID")) {
				Cookie cookieJ = new Cookie("JSESSIONID", "");
				cookieJ.setMaxAge(0);
				response.addCookie(cookieJ);
				//				cookies[i].setMaxAge(0);
				//				cookies[i].setPath("/");
				//				cookies[i].setValue("");
				//				((HttpServletResponse) response).addCookie(cookies[i]);
				System.out.println("sessionけした");
				response.addCookie(cookies[i]);
			} else if (cookies[i].getName().equals("mail")) {
				Cookie cookieMail = new Cookie("mail", "");
				cookieMail.setMaxAge(0);
				response.addCookie(cookieMail);
				//				cookies[i].setMaxAge(0);
				//				cookies[i].setPath("/");
				//				cookies[i].setValue("");
				//				((HttpServletResponse) response).addCookie(cookies[i]);
				//				response.addCookie(cookies[i]);
			} else if (cookies[i].getName().equals("password")) {
				Cookie cookieP = new Cookie("password", "");
				cookieP.setMaxAge(0);
				response.addCookie(cookieP);
				//				cookies[i].setMaxAge(0);
				//				cookies[i].setPath("/");
				//				cookies[i].setValue("");
				//				response.addCookie(cookies[i]);
			}
		}

		getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}
}
