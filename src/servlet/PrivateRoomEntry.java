package servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.JoinUserBean;
import beans.RoomBean;
import dao.JoinUserDAO;
import dao.RoomDAO;
import exception.DatabaseException;

/*
 * プライベートルーム
 */
@WebServlet("/PrivateRoomEntry")
public class PrivateRoomEntry extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JoinUserBean joinUserBean = new JoinUserBean();
		RoomBean roomBean = new RoomBean();
		RoomDAO roomDAO = new RoomDAO();
		String judge = request.getParameter("create");
		if (judge.equals("create")) {

			if (request.getParameter("roomType") == null) {
				roomBean.setRoomType("off");
			} else {
				roomBean.setRoomType(request.getParameter("roomType"));
			}
			roomBean.setRoomName(request.getParameter("roomName"));
			roomBean.setInvitation(request.getParameterValues("inviteCnt"));

			System.out.println("roomBean.getRoomType()の中は" + roomBean.getRoomType());
			if (roomBean.getRoomType() == "PRIVATE") {
				System.out.println("プライベートです。");
			} else {
				System.out.println("パブリックです。");
			}
			for (int i = 0; i < roomBean.getInvitation().length; i++) {
				System.out.println(roomBean.getInvitation()[i]);
				try {
					joinUserBean.setRoomId(JoinUserDAO.count());
					joinUserBean.setUserId(JoinUserDAO.nameSearch(roomBean.getInvitation()[i]));
					System.out.println(joinUserBean.getRoomId());
					System.out.println(joinUserBean.getUserId());
					JoinUserDAO.register(joinUserBean);
				} catch (DatabaseException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					System.out.println("DBがやばい！");
					e.printStackTrace();
				}
			}
			System.out.println(roomBean.getInvitation().length);
			System.out.println(roomBean.getRoomName());
			try {
				roomDAO.register(roomBean);
			} catch (DatabaseException e) {
				e.printStackTrace();
				//		System.out.println(RoomBean.getRoomType());
				getServletContext().getRequestDispatcher("/login.html").forward(request, response);
			} catch (SQLException e) {
				System.out.println("DBがやばい！");
				e.printStackTrace();
			}
		}
		getServletContext().getRequestDispatcher("/Index").forward(request, response);
	}
}
