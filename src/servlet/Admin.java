package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserBean;
import dao.UserDAO;
import exception.DatabaseException;

/**
 * Servlet implementation class Admin
 */
@WebServlet("/Admin")
public class Admin extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UserDAO userDAO = new UserDAO();
		ArrayList<UserBean> userBeanList = null;
		ArrayList<UserBean> allUserBeanList = null;
		try {
			userBeanList = userDAO.selectBanAll();
			allUserBeanList = userDAO.selectNotBanAll();
			System.out.println(userBeanList);
			System.out.println(allUserBeanList);
		} catch (DatabaseException e) {
			e.printStackTrace();
		}
		request.setAttribute("userBeanList", userBeanList);
		request.setAttribute("allUserBeanList", allUserBeanList);
		getServletContext().getRequestDispatcher("/WEB-INF/adminPage.jsp").forward(request, response);
	}
}
