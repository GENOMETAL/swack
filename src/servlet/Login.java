
package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.LoginSession;
import beans.SaveBean;
import beans.UserBean;
import dao.UserDAO;
import exception.DatabaseException;

@WebServlet("/Login")
public class Login extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UserBean userBean = (UserBean) request.getAttribute("userBean");
		System.out.println(userBean.getMail());
		System.out.println(userBean.getPassword());
		loginMethod(request, response, userBean);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UserBean userBean = new UserBean();
		userBean.setNo(request.getParameter("no"));
		userBean.setMail(request.getParameter("mail"));
		userBean.setPassword(request.getParameter("password"));
		SaveBean saveBean = new SaveBean();
		saveBean.setMail(request.getParameter("mail"));

		if (request.getParameter("checkCookie") != null) {//ログイン情報を保存するにチェックを入れてたら
			System.out.println("クッキー記録しまします");
			Cookie cookieMail = new Cookie("mail", request.getParameter("mail"));
			cookieMail.setMaxAge(60 * 5);
			response.addCookie(cookieMail);
			Cookie cookiePassword = new Cookie("password", request.getParameter("password"));
			cookiePassword.setMaxAge(60 * 5);
			response.addCookie(cookiePassword);
			System.out.println(cookieMail.getValue());
			System.out.println(cookiePassword.getValue());

		} else {
			Cookie cookieMail = new Cookie("Mail", "");
			cookieMail.setMaxAge(0);
			response.addCookie(cookieMail);
			System.out.println("nullset");

		}

		loginMethod(request, response, userBean);

	}

	private void loginMethod(HttpServletRequest request, HttpServletResponse response, UserBean userBean) throws ServletException, IOException {
		UserDAO userDAO = new UserDAO();
		try {
			if (!userDAO.select(userBean)) {//ログイン失敗
				System.out.println("login失敗したのでlogin,jspに戻ります（Login.java)から");
				request.setAttribute("userBean", userBean); //setしないとjsp側でbeanの値を取得できない

				getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
				return;
			}
		} catch (DatabaseException e) {
			e.printStackTrace();
		}
		HttpSession session = request.getSession();
		LoginSession loginSession = null;
		try {
			loginSession = userDAO.getUser(userBean);
		} catch (DatabaseException e) {
			e.printStackTrace();
		}
		session.setAttribute("loginSession", loginSession);
		request.setAttribute("userBean", userBean);
		//		getServletContext().getRequestDispatcher("/Index").forward(request, response);
		response.sendRedirect("Index?room=0");
		//getServletContext().getRequestDispatcher("/Index").forward(request, response);

	}
}