
package parameter;

public class Messages {
	private Messages() {
	}

	public static final String USER_PARAM_MISTAKE = "ユーザ情報に誤りがあります。入力項目を確認し、再度登録してください。";
	public static final String USER_ISENTRYED = "このユーザは登録済みです。もう一度入力し直してください。";
	public static final String USER_ENTRY_SUCCESS = "ワークスペースに参加しました。";
}