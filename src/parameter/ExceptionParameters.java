package parameter;

public class ExceptionParameters {
	public static final String SYSTEM_EXCEPTION_MESSAGE = "システムエラーが発生しました";
	public static final String DATABASE_CONNECTION_EXCEPTION_MESSAGE = "データベースへの接続時にエラーが発生しました";
	public static final String DATABASE_PROCESSING_EXCEPTION_MESSAGE = "データベースの処理中にエラーが発生しました";
	public static final String DATABASE_CLOSE_EXCEPTION_MESSAGE = "データベースからの切断時にエラーが発生しました";
}
