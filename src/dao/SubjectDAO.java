package dao;

import static parameter.DAOParameters.*;
import static parameter.ExceptionParameters.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import bean.SubjectBean;
import exception.DatabaseException;

public class SubjectDAO {

	public ArrayList<SubjectBean> selectAll() throws DatabaseException {
		ArrayList<SubjectBean> subjectList = new ArrayList<>();

		String sql = "SELECT * FROM subject";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD); Statement stmt = conn.createStatement(); ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					SubjectBean subjectBean = new SubjectBean();
					subjectBean.setSubjectId(rs.getInt("subjectid"));
					subjectBean.setSubjectName(rs.getString("subjectname"));
					subjectList.add(subjectBean);
				}
			}
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
		return subjectList;
	}

	public int register(SubjectBean subjectBean) throws DatabaseException {

		int cnt = 0;
		String sql = "INSERT INTO subject VALUES(?, ?)";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD); PreparedStatement pst = conn.prepareStatement(sql);) {
				pst.setInt(1, subjectBean.getSubjectId());
				pst.setString(2, subjectBean.getSubjectName());
				cnt = pst.executeUpdate();
			}
			System.out.println("INSERT INTO subject" + cnt + "件");
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
		return cnt;
	}

}
