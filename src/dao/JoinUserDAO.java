
package dao;

import static parameter.DAOParameters.*;
import static parameter.ExceptionParameters.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import beans.JoinUserBean;
import exception.DatabaseException;

public class JoinUserDAO {
	public static int register(JoinUserBean joinuserBean) throws DatabaseException {
		int cnt = 0;
		String sql = "INSERT INTO JOIN_USER VALUES(?, ?)";
		ArrayList joinuserBeanList = new ArrayList<>();
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD);
					PreparedStatement pst = conn.prepareStatement(sql);) {
				pst.setInt(1, (joinuserBean.getUserId()));
				pst.setInt(2, (joinuserBean.getRoomId()));
				cnt = pst.executeUpdate();
			}
			System.out.println("INSERT INTO suser" + cnt + "件");
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
		return cnt;
	}

	public static int count() throws DatabaseException, SQLException {
		int Rcount = 0;
		String sql = "SELECT COUNT(*) FROM ROOM";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD); Statement stmt = conn.createStatement(); ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					Rcount = rs.getInt(1);
				}
			}
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
		return Rcount;
	}

	public static int nameSearch(String name) throws DatabaseException, SQLException {
		int userNo = 0;
		String sql = "SELECT NO FROM SUSER WHERE NAME = ?";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD);
					PreparedStatement pst = conn.prepareStatement(sql);) {
				pst.setString(1, (name));
				ResultSet rs = pst.executeQuery();
				if (rs.next()) {
					userNo = rs.getInt("NO");
				}
			}
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
		return userNo;
	}
}