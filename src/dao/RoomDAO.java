
package dao;

import static parameter.DAOParameters.*;
import static parameter.ExceptionParameters.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import beans.LoginSession;
import beans.RoomBean;
import beans.SaveBean;
import exception.DatabaseException;

public class RoomDAO {
	public ArrayList selectAll() throws DatabaseException {
		ArrayList roomBeanList = new ArrayList<>();
		//UserBean userBean = new UserBean();
		SaveBean saveBean = new SaveBean();
		//LoginSession loginSession = new LoginSession();
		String mail = saveBean.getMail();
		System.out.println(mail);
		String sql = "SELECT * FROM ROOM A INNER JOIN JOIN_USER J ON A.ID = J.ROOMID WHERE J.USERID = '" + mail + "'ORDER BY ID ASC";//com@comのところをメアドに帰る
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD); Statement stmt = conn.createStatement(); ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					RoomBean roomBean = new RoomBean();
					roomBean.setRoomId(rs.getInt("ID"));
					roomBean.setRoomName(rs.getString("NAME"));
					roomBean.setRoomType(rs.getString("RTYPE"));

					System.out.println(roomBean.getRoomType());
					roomBeanList.add(roomBean);
				}
			}
		} catch (SQLException |

				ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
		return roomBeanList;
	}

	public int register(RoomBean roomBean) throws DatabaseException, SQLException {
		int cnt = 0;
		String sql = "INSERT INTO ROOM VALUES(?, ?, ?)";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD);
					PreparedStatement pst = conn.prepareStatement(sql);) {
				pst.setInt(1, count(roomBean));
				pst.setString(2, roomBean.getRoomName());
				if (roomBean.getRoomType() == "PUBLIC") {
					pst.setString(3, "PUBLIC");
				} else {
					pst.setString(3, "PRIVATE");
				}
				cnt = pst.executeUpdate();
				System.out.println("INSERT INTO ROOM" + cnt + "件");
			}
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
		return cnt;
	}

	public int count(RoomBean roomBean) throws DatabaseException, SQLException {
		int Rcount = 0;
		String sql = "SELECT COUNT(*) FROM ROOM";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD); Statement stmt = conn.createStatement(); ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					Rcount = rs.getInt(1);
				}
			}
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
		return Rcount;
	}

	public ArrayList<RoomBean> select(LoginSession loginSession) throws DatabaseException {
		ArrayList roomBeanList = new ArrayList<>();

		String sql = "SELECT * FROM (ROOM A  INNER JOIN JOIN_USER J ON A.ID = J.ROOMID) INNER JOIN SUSER S ON S.NO = J.USERID WHERE S.MAIL = '" + loginSession.getMail() + "'ORDER BY ID ASC";//com@comのところをメアドに帰る
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD); Statement stmt = conn.createStatement(); ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					RoomBean roomBean = new RoomBean();
					roomBean.setRoomId(rs.getInt("ID"));
					roomBean.setRoomName(rs.getString("NAME"));
					roomBean.setRoomType(rs.getString("RTYPE"));

					System.out.println(roomBean.getRoomType());
					roomBeanList.add(roomBean);
				}
			}
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
		return roomBeanList;

	}

	public RoomBean roomNameGetting(String roomId) throws DatabaseException, SQLException {
		RoomBean roomBean = new RoomBean();
		String sql = "SELECT * FROM ROOM WHERE ID = ?";
		try {
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD);
					PreparedStatement pst = conn.prepareStatement(sql);) {
				pst.setInt(1, Integer.parseInt(roomId));

				ResultSet rs = pst.executeQuery();
				if (rs.next()) {

					roomBean.setRoomId(Integer.parseInt(roomId));
					roomBean.setRoomName(rs.getString("NAME"));
					roomBean.setRoomType(rs.getString("RTYPE"));
					System.out.println("ルームゲット関数起動");
					System.out.println("部屋名→" + roomBean.getRoomName());
					System.out.println("部屋ID→" + roomBean.getRoomId());
				}
			}
		} catch (SQLException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
		return roomBean;
	}

	public ArrayList<RoomBean> selectAlldayo() throws DatabaseException {
		ArrayList roomBeanList = new ArrayList<>();
		String sql = "SELECT * FROM ROOM";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD); Statement stmt = conn.createStatement(); ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					RoomBean roomBean = new RoomBean();
					roomBean.setRoomId(rs.getInt("ID"));
					roomBean.setRoomName(rs.getString("NAME"));
					roomBean.setRoomType(rs.getString("RTYPE"));
					roomBeanList.add(roomBean);
				}
			}
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
		return roomBeanList;
	}
}