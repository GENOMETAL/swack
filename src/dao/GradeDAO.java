package dao;

import static parameter.DAOParameters.*;
import static parameter.ExceptionParameters.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import bean.GradeBean;
import exception.DatabaseException;

public class GradeDAO {

	public ArrayList<GradeBean> selectScoreAverageList() throws DatabaseException {
		ArrayList<GradeBean> scoreAverageList = new ArrayList<>();

		String sql = "SELECT subjectname, avg(score) FROM grade g JOIN subject s ON g.subjectid = s.subjectid GROUP BY subjectname";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD); Statement stmt = conn.createStatement(); ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					GradeBean gradeBean = new GradeBean();
					gradeBean.setSubjectName(rs.getString("subjectname"));
					gradeBean.setScoreAverage(rs.getDouble("avg(score)"));
					scoreAverageList.add(gradeBean);
				}
			}
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);

		}
		return scoreAverageList;
	}

	public int register(GradeBean gradeBean) throws DatabaseException {
		int cnt = 0;
		String sql = "INSERT INTO grade VALUES(?, ?, ?)";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD); PreparedStatement pst = conn.prepareStatement(sql);) {
				pst.setInt(1, gradeBean.getStudentNo());
				pst.setInt(2, gradeBean.getSubjectId());
				pst.setInt(3, gradeBean.getScore());
				cnt = pst.executeUpdate();
			}
			System.out.println("INSERT INTO grade" + cnt + "件");
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
		return cnt;
	}

}
