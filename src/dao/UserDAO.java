
package dao;

import static parameter.DAOParameters.*;
import static parameter.ExceptionParameters.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import beans.BanUserBean;
import beans.LoginSession;
import beans.UserBean;
import exception.DatabaseException;

public class UserDAO {
	public void delete_Ban(String name) throws DatabaseException, SQLException {
		String sql = "UPDATE suser SET JUDGMENT = NULL WHERE name = ?";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD);
					PreparedStatement pst = conn.prepareStatement(sql);) {
				pst.setString(1, name);
				pst.executeUpdate();

			}
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
	}

	public void add_Ban(String name) throws DatabaseException, SQLException {
		String sql = "UPDATE suser SET JUDGMENT = ? WHERE name = ?";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD);
					PreparedStatement pst = conn.prepareStatement(sql);) {
				pst.setString(1, "BAN");
				pst.setString(2, name);
				pst.executeUpdate();

			}
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
	}

	public ArrayList selectNotBanAll() throws DatabaseException {
		ArrayList allUserBeanList = new ArrayList<>();
		String sql = "SELECT * FROM SUSER WHERE judgment IS NULL ORDER BY NO ASC";

		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD); Statement stmt = conn.createStatement(); ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					BanUserBean banUserBean = new BanUserBean();

					banUserBean.setName(rs.getString("name"));
					banUserBean.setNo(rs.getString("no"));
					allUserBeanList.add(banUserBean);
				}
			}
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
		return allUserBeanList;

	}

	public ArrayList selectBanAll() throws DatabaseException {
		ArrayList userBeanList = new ArrayList<>();
		String sql = "SELECT * FROM SUSER WHERE judgment = 'BAN' ORDER BY NO ASC";

		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD); Statement stmt = conn.createStatement(); ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					UserBean userBean = new UserBean();

					userBean.setNo(rs.getString("no"));
					userBean.setName(rs.getString("name"));
					userBeanList.add(userBean);
				}
			}
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
		return userBeanList;

	}

	public ArrayList selectAll() throws DatabaseException {
		ArrayList userBeanList = new ArrayList<>();
		String sql = "SELECT * FROM SUSER ORDER BY NO ASC";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD); Statement stmt = conn.createStatement(); ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					UserBean userBean = new UserBean();
					userBean.setNo(rs.getString("No"));
					userBean.setName(rs.getString("name"));
					userBean.setMail(rs.getString("mail"));
					userBean.setPassword(rs.getString("password"));
					userBeanList.add(userBean);
				}
			}
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
		return userBeanList;
	}

	public int register(UserBean userBean) throws DatabaseException {
		int cnt = 0;
		String sql = "INSERT INTO suser VALUES(?, ?, ?,?,sysdate,default,default)";
		ArrayList userBeanList = new ArrayList<>();
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD);
					PreparedStatement pst = conn.prepareStatement(sql);) {
				pst.setInt(1, count(userBean));
				pst.setString(2, userBean.getName());
				pst.setString(3, userBean.getMail());
				pst.setString(4, userBean.getPassword());
				cnt = pst.executeUpdate();
			}
			System.out.println("INSERT INTO suser" + cnt + "件");
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
		sql = "INSERT INTO JOIN_USER VALUES(?, ?)";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD);
					PreparedStatement pst = conn.prepareStatement(sql);) {
				pst.setInt(1, count(userBean) - 1);
				pst.setInt(2, (0));
				cnt = pst.executeUpdate();
			}
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
		return cnt;
	}

	public int join_List_Register(UserBean userBean) throws DatabaseException {
		int cnt = 0;
		String sql = "INSERT INTO suser VALUES(?, ?, ?,?,sysdate)";
		ArrayList userBeanList = new ArrayList<>();
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD);
					PreparedStatement pst = conn.prepareStatement(sql);) {
				pst.setInt(1, count(userBean));
				pst.setString(2, userBean.getName());
				pst.setString(3, userBean.getMail());
				pst.setString(4, userBean.getPassword());
				cnt = pst.executeUpdate();
			}
			System.out.println("INSERT INTO suser" + cnt + "件");
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
		return cnt;
	}

	public boolean select(UserBean userBean) throws DatabaseException {
		String sql = "SELECT * FROM suser WHERE mail = ? AND password = ?";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD);
					PreparedStatement pst = conn.prepareStatement(sql);) {
				//				getUser(userBean);
				pst.setString(1, userBean.getMail());
				pst.setString(2, userBean.getPassword());
				ResultSet rs = pst.executeQuery();
				if (rs.next()) {
					if (LockCheck(userBean, getUser(userBean)) == false) {
						if (adnminCheck(getUser(userBean)) == true) {
							userBean.setAdmin(true);
							System.out.println("setAdmin");
						}
						userBean.setLoginFlag(false);
						System.out.println("ログインOK");

						return true;
					} else {
						userBean.setLoginCheck("NO");
						System.out.println("ブラックリストに登録されているためログインできんよ（笑）");
						userBean.setLoginFlag(true);

					}
				}
			}
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
		System.out.println("ログインNG");
		return false;
	}

	public LoginSession getUser(UserBean userBean) throws DatabaseException {
		String sql = "SELECT * FROM suser WHERE mail = ? AND password = ?";
		LoginSession loginSession = new LoginSession();
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD);
					PreparedStatement pst = conn.prepareStatement(sql);) {
				pst.setString(1, userBean.getMail());
				pst.setString(2, userBean.getPassword());
				ResultSet rs = pst.executeQuery();
				if (rs.next()) {
					loginSession.setNo(rs.getString("no"));
					loginSession.setMail(rs.getString("mail"));
					loginSession.setName(rs.getString("name"));
					System.out.println("loinseesion" + loginSession.getNo());
				}
			}
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
		return loginSession;
	}

	public int count(UserBean userBean) throws DatabaseException, SQLException {
		int Rcount = 0;
		String sql = "SELECT COUNT(*) FROM SUSER";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD); Statement stmt = conn.createStatement(); ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					Rcount = rs.getInt(1);
				}
			}
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
		return Rcount;
	}

	public boolean LockCheck(UserBean userBean, LoginSession loginSession) throws DatabaseException, SQLException {
		//		String sql = "SELECT * FROM locklist WHERE no = ? AND name = ?";					locklistテーブルを使わなくともいけそうなので藁
		String sql = "SELECT * FROM suser WHERE no = ? AND judgment = ?";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD);
					PreparedStatement pst = conn.prepareStatement(sql);) {
				pst.setString(1, loginSession.getNo());
				//				pst.setString(2, loginSession.getName());
				pst.setString(2, "BAN");
				ResultSet rs = pst.executeQuery();
				System.out.println(loginSession.getNo() + loginSession.getName());
				if (rs.next()) {
					System.out.println("rs.next");
					return true;
				}
			}
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
		System.out.println("truehe");
		return false;
	}

	public boolean adnminCheck(LoginSession loginSession) throws DatabaseException, SQLException {
		String sql = "SELECT * FROM suser WHERE no = ?  AND authority = ?";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD);
					PreparedStatement pst = conn.prepareStatement(sql);) {
				pst.setString(1, loginSession.getNo());
				pst.setString(2, "admin");
				ResultSet rs = pst.executeQuery();
				if (rs.next()) {
					System.out.println("adminユーザーです");
					return true;
				}
			}
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
		System.out.println("adminではない");
		return false;
	}
}