package dao;

import static parameter.DAOParameters.*;
import static parameter.ExceptionParameters.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import bean.StudentBean;
import exception.DatabaseException;

public class StudentDAO {

	public ArrayList<StudentBean> selectAll() throws DatabaseException {
		ArrayList<StudentBean> subjectList = new ArrayList<>();

		String sql = "SELECT * FROM student";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD); Statement stmt = conn.createStatement(); ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					StudentBean studentBean = new StudentBean();
					studentBean.setStudentNo(rs.getInt("studentno"));
					studentBean.setStudentName(rs.getString("studentname"));
					studentBean.setSchoolYear(rs.getInt("schoolyear"));
					subjectList.add(studentBean);
				}
			}
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
		return subjectList;
	}
}
