
package dao;

import static parameter.DAOParameters.*;
import static parameter.ExceptionParameters.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import beans.ChatBean;
import beans.RoomBean;
import beans.SaveBean;
import exception.DatabaseException;

public class ChatDAO {
	public ArrayList<ChatBean> select(String roomId) throws DatabaseException {
		ArrayList<ChatBean> chatBeanList = new ArrayList<>();
		int roomid = Integer.parseInt(roomId);
		System.out.println(roomId);
		String sql = "SELECT * FROM TESTCHAT WHERE ROOMID =" + roomid + "ORDER BY postdate ASC";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD); Statement stmt = conn.createStatement(); ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					ChatBean chatBean = new ChatBean();
					chatBean.setName(rs.getString("name"));
					chatBean.setMessage(rs.getString("message"));
					chatBean.setPostDate(rs.getString("postdate"));
					chatBeanList.add(chatBean);
				}
			}
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
		return chatBeanList;
	}

	public ArrayList selectAll(String roomId) throws DatabaseException {
		ArrayList chatBeanList = new ArrayList<>();
		SaveBean saveBean = new SaveBean();
		int roomid = Integer.parseInt(roomId);
		System.out.println(roomId);
		String sql = "SELECT * FROM TESTCHAT WHERE ROOMID =" + roomId + "ORDER BY postdate ASC";
		//String sql = "SELECT * FROM testchat WHERE ROOMID =" + room + "ORDER BY postdate ASC";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD); Statement stmt = conn.createStatement(); ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					ChatBean chatBean = new ChatBean();
					chatBean.setName(rs.getString("name"));
					chatBean.setMessage(rs.getString("message"));
					chatBean.setPostDate(rs.getString("postdate"));
					chatBeanList.add(chatBean);
				}
			}
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
		return chatBeanList;
	}

	public int register(ChatBean chatBean) throws DatabaseException {
		int cnt = 0;
		RoomBean roomBean = new RoomBean();
		SaveBean saveBean = new SaveBean();
		String sql = "INSERT INTO testchat VALUES(?, ?, ?,sysdate)";
		try {
			Class.forName(DRIVER_NAME);
			try (Connection conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD);
					PreparedStatement pst = conn.prepareStatement(sql);) {
				pst.setInt(1, chatBean.getRoomId());
				pst.setString(2, chatBean.getName());
				pst.setString(3, chatBean.getMessage());
				cnt = pst.executeUpdate();
			}
			System.out.println("INSERT INTO chat" + cnt + "件");
		} catch (SQLException | ClassNotFoundException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		}
		return cnt;
	}
}