
package validator;

import static parameter.Messages.*;

public class UserValidator {
	// 正規表現（半角英数字）
	private static final String MATCH_HANKAKUEISUU = "^[a-zA-Z0-9]+$";

	public static String checkPassword(String password) {
		if (password.isEmpty()) {
			/* 必須チェック */
			return USER_PARAM_MISTAKE;
		} else if (password.length() < 2 || password.length() > 8) {
			/* 桁数チェック */
			return USER_PARAM_MISTAKE;
		} else if (!password.matches(MATCH_HANKAKUEISUU)) {
			/* 文字種チェック */
			return USER_PARAM_MISTAKE;
		} else {
			/* 正常 */
			return "";
		}
	}
}