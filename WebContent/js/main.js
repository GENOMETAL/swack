var dy;

function loadingDisplayNone() {
  $("#loading").css("display", "none");
}

function containerDisplayInline() {
  $("#container").css("display", "inline");
  $(".message-history").scrollTop($(".message-history")[0].scrollHeight);
  dy = $(".message-history").scrollTop();
}

$(window).on('load', function() {
  if ('on' == $("#postFlg").val()) {
    containerDisplayInline();
  } else {
    setTimeout(function() {
      loadingDisplayNone();
    }, 1500);
    setTimeout(function() {
      containerDisplayInline();
    }, 2000);
  }

//  setInterval(function() {
//    var y = $(".message-history").scrollTop();
//    $('.main').load('Index .main', function() {
//      if (dy != y) {
//        $(".message-history").scrollTop(y);
//      } else {
//        $(".message-history").scrollTop($(".message-history")[0].scrollHeight);
//      }
//    })
//  }, 15000); // 15秒間隔で更新
});

$(function() {
  $('#message').attr('autofocus', 'autofocus');
  $("input[type=text]").keypress(function(ev) {
    if ((ev.which && ev.which === 13) || (ev.keyCode && ev.keyCode === 13)) {
      return false;
    } else {
      return true;
    }
  });
  if ($("#message").val().length == 0) {
    $("#send").css("color", "rgba(44,45,48,.75)");
    $("#send").css("background", "#e8e8e8");
    $("#send").prop("disabled", true);
  }
  $("#message").on("keydown keyup keypress change", function() {
    if ($(this).val().length < 1) {
      $("#send").css("color", "rgba(44,45,48,.75)");
      $("#send").css("background", "#e8e8e8");
      $("#send").prop("disabled", true);
    } else {
      $("#send").css("color", "#ffffff");
      $("#send").css("background", "#008952");
      $("#send").prop("disabled", false);
    }
  });
  $('#message').keydown(function(e) {
    // ctrlキーが押されてる状態か判定
    if (event.ctrlKey) {
      // 押されたキー（e.keyCode）が13（Enter）かそしてテキストエリアに何かが入力されているか判定
      if (e.keyCode === 13 && $(this).val()) {
        // フォームを送信
        $('#sendMessage').submit();
        return false;
      }
    }
  });
});