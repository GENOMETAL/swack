<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="beans.RoomBean"  %>
<jsp:useBean id="roomBean" scope="request" class="beans.RoomBean"></jsp:useBean>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<title>Swackサンプル</title>
<link rel="stylesheet" type="text/css" href="css/style.css">
<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="js/main.js"></script>
</head>
<body>
	<div id="loading">
		<div class="loading_message">${loadingMessageBean.loadingMessage}</div>
		<div class="loading_message_min">－Swackのヒトコト</div>
		<div class="loading_img">
			<img src="img/loading.gif">
		</div>
	</div>
	<div id="container">
		<div class="header">
			<div class="team-menu">SJ2WAC1Aコース</div>
			<div class="room-menu">
				<span class="room-menu_name">
					<img class="room-menu_prefix" src="img/sb.png" /><jsp:getProperty property="roomName" name="roomBean"/><a href="Member?roomId=${roomBean.roomId }" title="メンバーを追加する"><img class="room-memberadd_img" src="img/ma.png" /> <span class="membercnt">45</span></a>
				</span>
			</div>
		</div>
		<div class="main">
			<div class="listings">
				<div class="listings_rooms">
					<h2 class="listings_header">
						ルーム <a href="Room" title="ルームを作成する"><img class="listings_header_img" src="img/p.png" /></a>
					</h2>
					<ul class="room_list">
						<c:forEach var="roomBean" items="${roomBeanList}">


						<a href="Index?room=${roomBean.roomId}">
						        <span class="prefix">
						        	<c:if test="${roomBean.roomType == 'PRIVATE'}"><img src="img/k.png" /></c:if>
									<c:if test="${roomBean.roomType == 'PUBLIC'}"><img src="img/s.png" /></c:if>
									</span>
									<c:out value="${roomBean.roomName}" /></a>
									<br>

			    		</c:forEach>
					</ul>
					<hr>


				</div>
				<div class="listings_direct-messages">
					<h2 class="listings_header">
						ダイレクトメッセージ <img class="listings_header_img" src="img/p.png" />
					</h2>
					<ul class="room_list">
						<li class="room"><a class="room_name"><span>
									<span class="prefix">
										<img src="img/h.png" />
									</span>
									swackbot
								</span></a></li>
						<li class="room"><a class="room_name"><span>
									<span class="prefix">
										<img src="img/y.png" />
									</span>
									情報太郎（自分）
								</span></a></li>
						<li class="room"><a class="room_name"><span class="unread">2</span> <span>
									<span class="prefix">
										<img src="img/n.png" />
									</span>
									ユーザ１
								</span></a></li>
						<li class="room"><a class="room_name"><span class="unread">1</span> <span>
									<span class="prefix">
										<img src="img/y.png" />
									</span>
									ユーザ２
								</span></a></li>
						<li class="room"><a class="room_name"><span>
									<span class="prefix">
										<img src="img/n.png" />
									</span>
									ユーザ３
								</span></a></li>
					</ul>
				</div>
				<c:if test="${userBean.admin}" var="flg" >
				<hr>
				<a href="Admin" title="admin"><font color="red"> <font size="5">管理者用ページ</font></font></a></c:if>



			</div>


			<div class="message-history">
				<c:forEach var="chatBean" items="${chatBeanList}">
			    <hr>
			    <div class="message">
			        <span class="message_profile-pic"></span><span class="message_username"><c:out value="${chatBean.name}" /></span>
			        <span class="message_timestamp"><c:out value="${chatBean.postDate}" /></span>
			        <span class="message_star"></span>
			        <span class="message_content"> <c:out value="${chatBean.message}" /> </span>
			    </div>
			    </c:forEach>
			</div>
			</div>
		</div>
		<div class="footer">


<div class="user-menu">
    <a href="CookieDelete" title="Swackからサインアウトする"> <span class="user-menu_profile-pic"></span> <span class="user-menu_username"><c:out value="${loginSession.name}" /></span> <span class="connection_status">ログイン中</span>
    </a>
</div>
			<form action="ChatEntry" method="post" id="sendMessage">
				<div class="input-box">
					<% String room = (String) request.getAttribute("room"); %>
					 <input type="hidden" name="roomid" value="<%=room%>">
					<input class="input-box_text" type="text" id="message" name="message" placeholder="#everyoneへのメッセージ" autocomplete="off" />
					<input class="input-box_button" type="submit" id="send" value="送信" />
				</div>
			</form>
		</div>

</body>
</html>