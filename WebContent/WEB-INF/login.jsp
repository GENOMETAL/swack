<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン</title>
<link href="css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link rel="stylesheet" type="text/css" href="css/login.css">
<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container form-container">
		<div class="row">


<c:if test="${!empty infoMsg}">
  <div class="alert alert-success">
    <strong>Success!</strong><c:out value="${infoMsg}" />
  </div>
</c:if>
<c:if test="${!empty exceptionMsg}">
  <div class="alert alert-danger">
    <strong>Exception!</strong><c:out value="${exceptionMsg}" />
  </div>
</c:if>

			<div class="col-md-6 login-form">
				<h3>Swackにログイン</h3>
				<h5>メールアドレスとパスワードを入力してください。</h5>
				<form action="Login" method="post">
					<div class="form-group">
						<input type="email" name="mail" id="loginInputEmail" class="form-control" placeholder="you@example.com" required autofocus />
					</div>
					<div class="form-group">
						<input type="password" name="password" id="loginInputPassword" class="form-control" placeholder="パスワード" required />
					</div>

					<c:if test="${userBean.loginCheck == 'NO'}" var="flg" ><h5><font color="red">ブラックリストに登録されているためログインできません</font></h5></c:if>

					<div class="form-group">
						<input type="submit" class="btnSubmit" value="ログイン" />
					</div>
					<div id="remember" class="checkbox">
						<label> <input type="checkbox" name="checkCookie" value="remember-me">ログイン情報を記録する
						</label>
					</div>
				</form>
			</div>
			<div class="col-md-6 entry-form">
				<h3>Swackワークスペースに参加する</h3>
				<h5>氏名、メールアドレス、パスワードを入力してください。</h5>
				<form action="UserEntry" method="post">

<c:if test="${!empty userBean.errMsg}">
            <div class="alert alert-warning">
                <strong>Error!</strong>
                <c:out value="${userBean.errMsg}" />
            </div>
        </c:if>

					<div class="form-group">
						<input type="text" id="inputName" name="name" class="form-control" placeholder="氏名"value="${userBean.name}" required />
					</div>
					<div class="form-group">
						<input type="email" id="inputEmail" name="mail" class="form-control" placeholder="you@example.com" value="${userBean.mail}"required />
					</div>
					<div class="form-group">
						<input type="password" id="inputPassword" name="password" class="form-control" placeholder="パスワード" required />
					</div>
					<div class="form-group">
						<input type="submit" class="btnSubmit" value="参加する" />
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>