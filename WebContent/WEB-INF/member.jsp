<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ルーム</title>
<link href="css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link href="css/bootstrap-select.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/member.css">
<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="js/member.js"></script>
</head>
<body>
	<div class="container form-container">
		<div class="row">
			<div class="col-md-12 member-form">
				<h3>他のユーザを招待する</h3>
				<form action="RoomJoin" method="post"><input type="hidden" name="roomId" value="${roomBean.roomId }">
					<div class="form-group">
						<label class="control-label">招待の送信先:(任意)</label> <select class="form-control selectpicker" name="inviteCnt" data-live-search="true" data-selected-text-format="count > 1" multiple>
							<c:forEach var="userBean" items="${userBeanList}">
						        <option value="${userBean.no}">${userBean.name}</option>
			    			</c:forEach>
						</select>
						<span class="users-note">このルームに追加したい人を選んでください。</span>
					</div>
					<div class="room-form-btn">
						<button name="create" value="cansel"  class="btn btn-default">キャンセル</button>
						<button name="create" value="create" id="send" class="btn btn-default">ルームを作成する</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>