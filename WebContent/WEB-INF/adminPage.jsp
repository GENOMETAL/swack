<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ルーム</title>
<link href="css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link href="css/bootstrap-toggle.min.css" rel="stylesheet">
<link href="css/bootstrap-select.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/room.css">
<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/bootstrap-toggle.min.js"></script>
<script type="text/javascript" src="js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="js/admin.js"></script>
</head>
<body>
	<div class="container form-container">
		<div class="row">
			<div class="col-md-12 room-form">
				<h3>管理者用ページ</h3>
				<p class="input_note_special medium_bottom_margin">
					ここではロックされたアカウントを解除することができます。<br> また、アカウントをロックすることができます
				</p>
				<form action="AuthorityManagement" method="post">
					<div class="form-group">
						<div>
							<label><input type="checkbox" id="chk" name="roomType"
								checked data-toggle="toggle" data-on="解除" data-off="ロック"
								data-onstyle="success" data-offstyle="warning"></label> <span
								class="toggle_label">このルームは、ワークスペースのメンバーであれば誰でも閲覧・参加することができます。</span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label">適用するユーザ</label>
						<div class="aaa">
							<select class="form-control selectpicker" name="approval"
								id="name" data-live-search="true"
								data-selected-text-format="count > 1" multiple>


								<c:forEach var="userBean" items="${userBeanList}">
									<option value="${userBean.name}">${userBean.name}</option>
								</c:forEach>
							</select>
						</div>
						<div class="bbb">
							<select class="form-control selectpicker" name="ban" id="name"
								data-live-search="true" data-selected-text-format="count > 1"
								multiple>
								<c:forEach var="banUserBean" items="${allUserBeanList}">
									<option value="${banUserBean.name}">${banUserBean.name}</option>
								</c:forEach>

							</select>
						</div>
						<span class="users-note">適用したいユーザーを選択してください</span>
					</div>
					<div class="room-form-btn">
						<button class="btn btn-default">キャンセル</button>
						<button id="send" class="btn btn-default">実行する</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(".aaa").css("display", "block");
		$(".bbb").css("display", "none");
	</script>
</body>
</html>